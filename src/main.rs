use argh::FromArgs;
use std::error::Error;
use std::fmt::Write;
use std::net::TcpStream;
use std::path::PathBuf;
use tracing_subscriber::prelude::*;
use tracing_subscriber::{fmt, EnvFilter};

use image::io::Reader as ImageReader;

#[derive(Debug, FromArgs)]
/// send Pixelflut image
struct PixOpts {
    /// udp
    #[argh(switch)]
    udp: bool,

    /// the Url
    #[argh(positional)]
    url: String,

    /// the image path
    #[argh(positional)]
    image: PathBuf,

    /// flush after every n pixels
    #[argh(option)]
    flush: Option<usize>,

    /// wdth of the canvas
    #[argh(option)]
    width: Option<u32>,

    /// height of the canvas
    #[argh(option)]
    height: Option<u32>,

    /// x offset
    #[argh(option, default = "0")]
    xo: u32,

    /// y offset
    #[argh(option, default = "0")]
    yo: u32,
}

fn image_to_string(opts: &PixOpts) -> Result<String, Box<dyn Error>> {
    let im = ImageReader::open(&opts.image)?.decode()?.to_rgb8();
    let mut string = String::new();
    for (x, y, &px) in im.enumerate_pixels() {
        if x >= opts.width.unwrap_or(u32::MAX) {
            continue;
        }
        if y >= opts.height.unwrap_or(u32::MAX) {
            continue;
        }
        write!(
            string,
            "PX {} {} {:02x}{:02x}{:02x}\n",
            opts.xo + x,
            opts.yo + y,
            px.0[0],
            px.0[1],
            px.0[2]
        )?;
    }
    Ok(string)
}

fn stream_image(img: &str, writer: &mut dyn std::io::Write) -> Result<(), Box<dyn Error>> {
    let mut loopcnt = 0;
    loop {
        writer.write_all(img.as_bytes())?;
        loopcnt += 1;
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    tracing_subscriber::registry()
        .with(fmt::layer())
        .with(EnvFilter::from_default_env())
        .init();
    let opts: PixOpts = argh::from_env();

    let image = image_to_string(&opts)?;
    println!("{}", &opts.url);
    //let file = BufWriter::new(File::create("testcase.txt")?);
    //loop_impl(opts, file)?;

    let mut socket = TcpStream::connect(&opts.url)?;
    stream_image(&image, &mut socket)?;

    Ok(())
}
